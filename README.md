# 微信小程序 开源

#### 项目介绍
微信小程序 开源  不像很多人一样代码放上面有很多坑，本程序是借鉴码云其他开源项目。把所有坑都已经躺过了，可以直接使用。后台代码还在开发整理中。如需帮助请加qq群：877611106。

## 技术选型
* 1 后端使用技术
    * 1.1 springframework4.3.7.RELEASE
    * 1.2 mybatis3.4.1
    * 1.3 shiro1.3.2
    * 1.4 servlet3.1.0
    * 1.5 druid1.0.28
    * 1.6 slf4j1.7.19
    * 1.7 fastjson1.2.30
    * 1.8 poi3.15
    * 1.9 velocity1.7
    * 1.10 quartz2.2.3
    * 1.11 mysql5.1.39
    * 1.12 swagger2.4
    * 1.13 j2cache2.3.22-release
        
* 2 前端使用技术
    * 2.1 Vue2.5.1
    * 2.2 iview
    * 2.3 layer3.0.3
    * 2.4 jquery2.2.4
    * 2.5 bootstrap3.3.7
    * 2.6 jqgrid5.1.1
    * 2.7 ztree3.5.26
    * 2.8 froala_editor1.2.2

## 页面展示
### 个人中心
![](http://pgf1db9j6.bkt.clouddn.com/Advertisement1.png "个人中心")
### 专题
![](http://pgf1db9j6.bkt.clouddn.com/Advertisement2.png "专题")
### 分类
![](http://pgf1db9j6.bkt.clouddn.com/Advertisement3.png "分类")
### 演示地址
![](http://pgf1db9j6.bkt.clouddn.com/Advertisement4.jpg "演示地址")